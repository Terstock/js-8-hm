// Теоретичні питання
// 1. Опишіть своїми словами як працює метод forEach.

//Метод виконує певну дію для кожного елемента масиву по черзі

// 2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

//Мутують: push(), pop(), unshift(),shift(), splice()
//Повертають: slice(), concat(), filter(), find() ,  reduce(), sort(), reverse(), join()

// 3. Як можна перевірити, що та чи інша змінна є масивом?

//Через метод isArray()

// 4. В яких випадках краще використовувати метод map(), а в яких forEach()?

//Якщо ми хочемо повернути новий масив, то краще використовувати метод map(), так як метод forEach виконує певну дію для кожного елемента масиву по черзі

// Практичні завдання
// 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи.
// Вивести це число в консоль.

// let array = ["travel", "hello", "eat", "ski", "lift"];
// let counter = 0;

// for (let i = 0; i < array.length; i++) {
//   if (array[i].length > 3) {
//     counter++;
//   }
// }
// console.log(`Кількість рядків більше за 3 символи: ${counter}`);

// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
// Наповніть різними даними.Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
// Відфільтрований масив виведіть в консоль.

// let people = [
//   { name: "Іван", age: 25, sex: "чоловіча" },
//   { name: "Ганна", age: 32, sex: "жіноча" },
//   { name: "Петро", age: 44, sex: "чоловіча" },
//   { name: "Анастасія", age: 28, sex: "жіноча" },
// ];

// function sortPeople(mass) {
//   return mass.filter((man) => man.sex === "чоловіча");
// }

// console.log(sortPeople(people));

// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
// Технічні вимоги:
// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.
// Тобто якщо передати масив['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив[23, null].

let array = ["hello", "world", 23, "23", null, true, 19, "responsible"];
let param = "krkr";

function filterBy(mass, delParam) {
  mass = mass.filter((elem) => typeof delParam !== typeof elem);

  return mass;
}

console.log(filterBy(array, param));
